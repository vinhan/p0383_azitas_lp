<?php
///////////////////////////////////////////////////////////////////////////
/// config.phpの内容をオーバーライドする場合
/// （条件によって送信先を変えたい。バリデーションルールを変えたい。）
/// はここで記述すること（ソースが見難くならないように。）
/// フォームからのパラメータによるif分岐などは$_SESSIONを利用してください。
/// ex)
/// if (!empty($_SESSION['hoge'])) {
///  	add_valid_rule('estimate', 'required', 'お見積り内容は必須です。');
/// }
///////////////////////////////////////////////////////////////////////////
if(!empty($_POST['kiboudate'])) {
	if($_POST['kiboudate'] !== '来場予定無し/未定') {
		add_valid_rule(array('kiboutime'), 'required', '商談希望時間は選択必須項目です');
	}
}
if(!empty($_POST['report'])) {
	if($_POST['report'] === '競合分析レポート') {
		add_valid_rule(array('site'), 'required', '競合サイトのURLは必須項目です');
	}
}
