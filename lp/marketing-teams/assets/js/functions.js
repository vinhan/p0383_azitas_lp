/*
 * matchheight
 */

$(document).ready(function() {
    $('.c-list__ttl').matchHeight();

    $('.p-top01__img--quote').each(function() {
        $(this).yurayura({
            'move': 15,
            'delay': 100,
            'duration': 1000
        });
    })

    var report_check = $('.checklist-input input[type="radio"]');

    report_check.on('click', function() {
        if ($('.input-check').is(':checked')) {
            $(this).parent('.check-url__input').find('.p-form_tbl_cell__listHide').show();
        } else {
            $('.p-form_tbl_cell__listHide').hide();
            $('.p-form_tbl_cell__listHide input').prop('checked', false);
        }
    });

    for (let i = 0; i < report_check.length; i++) {
        var _self = report_check.eq(i);
        if (_self.hasClass('input-check') && _self.is(':checked')) {
            _self.parent('.check-url__input').find('.p-form_tbl_cell__listHide').show();
        }
    }

    // 来場希望日
    var radio_date = $('input[name="kiboudate"]'),
        select_time = $('select[name="kiboutime"]'),
        opt = select_time.find('option'),
        first_elm = [];

    for (var i = 0; i < opt.length; i++) {
        var elm = opt.eq(i).html();
        first_elm.push(elm);
    }

    radio_date.on('change', function() {
        var val = $(this).val(),
            count = opt.length;

        if (val == '来場予定無し/未定') {
            $(this).parents('tr').next('tr').hide();
        } else {
            $(this).parents('tr').next('tr').show();
        }

        if (val == '10月25日(金)') {
            count = opt.length - 2;
        }

        select_time.empty();
        for (var i = 0; i < count; i++) {
            var label = first_elm[i],
                value = first_elm[i],
                text = first_elm[i];

            if (i == 0) {
                value = "";
            }
            var elm = '<option label="' + label + '" value="' + value + '">' + text + '</option>';

            select_time.append(elm);
        }
    });

    $(window).on('load', function() {
        if ($('input[name="kiboudate"]:checked').val() == '来場予定無し/未定') {
            radio_date.parents('tr').next('tr').hide();
        } else {
            radio_date.parents('tr').next('tr').show();
        }
    });
});

/* Back to top
------------------------------------------------------------*/



if ($(".c-backtop").length) {
    $(window).scroll(function() {
        if ($(this).scrollTop()) {
            $(".c-backtop").fadeIn(500);
            $(".c-backtop").addClass("is-show");
        } else {
            $(".c-backtop").fadeOut(800);
            setTimeout(function() {
                $(".c-backtop").removeClass("is-show");
            }, 100);
        }
    });

    $(window).scroll(function() {
        $(".c-backtop").removeClass("is-show");
        if ($(window).scrollTop() + $(window).height() > ($(document).height() - 86)) {
            $(".c-backtop").addClass("is-show");
        }
    });

    $(".c-backtop").click(function() {
        $("html, body").animate({ scrollTop: 0 }, 1000);
    });
}



// $(function(){
//     var paper_check_on = $(".is-study li input[type=radio]:checked");
//     $(paper_check_on).parent().parent().addClass('active');

//     $('.is-study li').off('click');
//     $(this).on('click', function() {
//         if(!$(this).hasClass('active')){
//             $(this).addClass('active');
//             $(this).siblings().removeClass('active');
//         }
//     });
// });

// $(function(){

//     var demand_check_on = $(".is-check li input[type=checkbox]:checked");
//     $(demand_check_on).parent().parent().addClass('active');


//     $('.is-check li').off('click');
//     $(this).on('click', function() {
//         console.log($(this));
//         $(this).toggleClass('active');

//         if($(this).hasClass('active')){
//             $(this).removeClass('active');
//         }else{
//             $(this).addClass('active');
//         }

//     });
// });