$(function(){
	$('.tab').click(function(){
		var index = $('.tab').index(this);
		$('.tab').removeClass('active');
		$(this).addClass('active');
		$('.txt').removeClass('active');
		$('.txt').eq(index).addClass('active');
	});

	$('input[name="visit[]"]').parent('label').click(function() {
		const input = $(this).find('input');
		const value = input.val();

		if(value === '未定' || value === '来場予定なし(別日訪問希望)') {
			$('input[name="visit[]"]').not(input).prop('checked', '');
		} else {
			$('input[name="visit[]"]').each(function(index, el) {
				const value = $(this).val();

				if(value === '未定' || value === '来場予定なし(別日訪問希望)') {
					$(this).prop('checked', '');
				}
			});
		}
	});

	$('.accordion_ttl').click(function(){
		$(this).toggleClass('active');
		$(this).next('.accordion_txt').slideToggle(300);
	});
});