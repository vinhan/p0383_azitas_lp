<?php
/**
 * メールフォーム基幹処理クラス
 *
 * POSTされたパラメータをもとにモードを振り分ける。
 *
 * LICENSE: ライセンスに関する情報
 *
 * @category   Respect
 * @package    mailform
 * @subpackage none
 * @license    http://www.opensource.org/licenses/gpl-2.0.php GNU General Public License, version 2
 * @copyright  Copyright (c) 2011 Respect(http://www.respect-pal.jp/)
 * @link       
 * @version    
 * @since      File available since Release 0.1.0
 * @author     Suguru Chiba @ Respect
 */

class MailForm
{

//////////////////////////////////////////////////////////////////////////////////
////// クラス内定数
//////////////////////////////////////////////////////////////////////////////////

// Nothing Now

//////////////////////////////////////////////////////////////////////////////////
////// プロパティ
//////////////////////////////////////////////////////////////////////////////////

	/**
	 * カレントディレクトリ
	 */
//	var $current_dir;

	/**
	 * ユーザからの処理要求
	 * confirm : 確認画面へ行きたい
	 * send    : メール送信処理をしてサンキュー画面へ行きたい
	 */
	var $mode = 'confirm';

	/**
	 * 処理結果による結果
	 * index       : 最初の画面
	 * error       : エラー画面
	 * confirm     : 確認画面
	 * retry_input : 最初の画面（ただしセッション維持）
	 * thanks      : サンキュー画面
	 */
	var $process = 'index';

	/**
	 * フォームからの送信パラメータ
	 */
	var $post = null;

	/**
	 * 出力データ
	 */
	var $output = null;

	/**
	 * セッションの名称
	 */
	var $session_name = 'MFSESSID';

	/**
	 * Validatorオブジェクト
	 */
	var $validator = null;

	/**
	 * エラーリザルト
	 */
	var $errors = array();

	/**
	 * ファイル（このスクリプトやテンプレート）のエンコードタイプ
	 * デフォルトはUTF-8にしておくこと
	 */
	var $file_encode_type = FILE_ENCODE_TYPE;

	/**
	 * キャラクターセット（実際表示する）のエンコードタイプ
	 */
	var $char_encode_type = CHAR_ENCODE_TYPE;

	/**
	 * ポストパラメータのエンコードタイプ
	 */
	var $posted_encode_type = null;

	/**
	 * PHPMailerオブジェクト
	 */
	var $phpmailer = null;

	/**
	 * Smartyオブジェクト
	 */
	var $smarty = null;

	/**
	 * Utilオブジェクト
	 */
	var $Util = null;

	/**
	 * Smartyのテンプレートディレクトリ
	 * index.phpからの相対パスで指定
	 */
	var $template_dir = '/./templates';

	/**
	 * Smartyのコンパイルディレクトリ
	 * index.phpからの相対パスで指定
	 */
	var $compile_dir = '/./templates_c';

	/**
	 * Smartyの開始デリミタ
	 */
	var $left_delimiter = '<!--{';

	/**
	 * Smartyの終了デリミタ
	 */
	var $right_delimiter = '}-->';


//////////////////////////////////////////////////////////////////////////////////
////// アクセサメソッド
//////////////////////////////////////////////////////////////////////////////////
	/**
	 * $mode のアクセサメソッド
	 */
	function setMode($mode) {
		$this->mode = $mode;
		$_SESSION['__mode__'] = $mode;
	}

	function getMode() {
		return $this->mode;
	}

	/**
	 * $process のアクセサメソッド
	 */
	function setProcess($process) {
		$this->process = $process;
	}

	function getProcess() {
		return $this->process;
	}

	/**
	 * $post のアクセサメソッド
	 */
	function setPost($post) {
		$this->post = $post;
		return;
	}

	function getPost() {
		return $this->post;
	}

	/**
	 * $session_name のアクセサメソッド
	 */
	function setSessionName($session_name) {
		$this->session_name = $session_name;
	}

	function getSessionName() {
		return $this->session_name;
	}

	/**
	 * $errors のアクセサメソッド
	 */
	function setErrors($errors) {
		$this->errors = $errors;
	}

	function getErrors() {
		return $this->errors;
	}

	/**
	 * $output のアクセサメソッド
	 */
	function setOutput($output) {
		$this->output = $output;
	}

	function getOutput() {
		return $this->output;
	}

	/**
	 * $phpmailer のアクセサメソッド
	 */
	function setPhpmailer($phpmailer) {
		$this->phpmailer = $phpmailer;
		return;
	}

	function getPhpmailer() {
		return $this->phpmailer;
	}

	/**
	 * $smarty のアクセサメソッド
	 */
	function setSmarty($smarty) {
		$this->Smarty = $smarty;
	}

	function getSmarty() {
		return $this->Smarty;
	}

	/**
	 * $util のアクセサメソッド
	 */
/*
	function setUtil($util) {
		$this->Util = $util;
		return;
	}

	function getUtil() {
		return $this->Util;
	}
*/
	/**
	 * $validator のアクセサメソッド
	 */
	function setValidator($validator) {
		$this->validator = $validator;
	}

	function getValidator() {
		return $this->validator;
	}

	/**
	 * $file_encode_type のアクセサメソッド
	 */
	function setFileEncodeType($file_encode_type) {
		$this->file_encode_type = $file_encode_type;
	}

	function getFileEncodeType() {
		return $this->file_encode_type;
	}

	/**
	 * $char_encode_type のアクセサメソッド
	 */

	function setCharEncodeType($char_encode_type) {
		$this->char_encode_type = $char_encode_type;
	}

	function getCharEncodeType() {
		return $this->char_encode_type;
	}

	/**
	 * $posted_encode_type のアクセサメソッド
	 */
	function setPostedEncodeType($posted_encode_type) {
		$this->posted_encode_type = $posted_encode_type;
	}

	function getPostedEncodeType() {
		return $this->posted_encode_type;
	}

	/**
	 * $template_dir のアクセサメソッド
	 */
	function setTemplateDir($template_dir) {
		$this->template_dir = $template_dir;
	}

	function getTemplateDir() {
		return $this->template_dir;
	}

	/**
	 * $compile_dir のアクセサメソッド
	 */
	function setCompileDir($compile_dir) {
		$this->compile_dir = $compile_dir;
	}

	function getCompileDir() {
		return $this->compile_dir;
	}

	/**
	 * $left_delimiter のアクセサメソッド
	 */
	function setLeftDelimiter($left_delimiter) {
		$this->left_delimiter = $left_delimiter;
	}

	function getLeftDelimiter() {
		return $this->left_delimiter;
	}

	/**
	 * $right_delimiter のアクセサメソッド
	 */
	function setRightDelimiter($right_delimiter) {
		$this->right_delimiter = $right_delimiter;
	}

	function getRightDelimiter() {
		return $this->right_delimiter;
	}


//////////////////////////////////////////////////////////////////////////////////
////// メソッド（基幹）
////// [処理の大まかな流れ]
////// MailFrom()			: コンストラクタ
////// ↓
////// init()				: 初期設定メソッド
////// ↓
////// beforeFilter()		: 処理実行前データフィルター
////// ↓
////// produce()			: 処理の振り分け基底メソッド
////// ↓
////// render()				: 最終処理(レンダリング)基底メソッド
////// ↓
////// afterFilter()		: レンダリング前最終データフィルター
////// 
//////////////////////////////////////////////////////////////////////////////////

	/**
	 * コンストラクタ
	 * 
	 */
	function MailForm($post) {

		$this->sessionStart();

		$this->init();

		$this->beforeFilter();

		$post = (!empty($post)) ? $post : null;

		// ポストパラメータの文字コード検出
		if (isset($post['detect_encoding']) && !empty($post['detect_encoding'])) {
			mb_detect_order('ASCII,JIS,UTF-8,eucJP-win,SJIS-win');
			$posted_encode_type = mb_detect_encoding($post['detect_encoding']);
			$this->setPostedEncodeType($posted_encode_type);
		}

		// エンコード処理にかける
		if (!is_null($this->getPostedEncodeType())) {

			$from_encoding = $this->getPostedEncodeType();
		} else {
			$from_encoding = $this->getCharEncodeType();
		}
		$to_encoding = $this->getFileEncodeType();
		mb_convert_variables($to_encoding, $from_encoding, $post);

		// データの標準化
		$post = Util::normalize($post);
		// プロパティへセット
		$this->setPost($post);
	}

	/**
	 * データ受け取り時点でのフィルター
	 * 
	 */
	function beforeFilter() {
		// 追加処理と追加ルールの呼び出し
		require_once THIS_ROOT . '/./expansion.php';
	}

	/**
	 * データ出力前のフィルター
	 * 
	 */
	function afterFilter() {

	}

	/**
	 * 基本設定
	 * 
	 */
	function init() {
		// クラスファイルの呼び出し
		require_once LIB_DIR . 'Smarty/libs/Smarty.class.php';
		require_once LIB_DIR . 'Class.Util.php';

		// Smartyの呼び出しと基本設定
		$smarty = new Smarty();
		$smarty->template_dir = THIS_ROOT . $this->getTemplateDir();
		$smarty->compile_dir = THIS_ROOT . $this->getCompileDir();
		$smarty->left_delimiter = $this->getLeftDelimiter();
		$smarty->right_delimiter = $this->getRightDelimiter();
		//smarty 拡張form pluginを使うために必要とのこと。
		$smarty->load_filter("pre","form");
		// 初期設定を終えたインスタンスをセットする
		$this->setSmarty($smarty);
	}

	/**
	 * 実行処理
	 * 
	 */
	function execute() {
		// 現在のモードの確認
		if (isset($_SESSION['__mode__'])) {
			$this->setMode($_SESSION['__mode__']);
		}
		// フィルターにかける
		$this->beforeFilter();

		// モード振り分け
		$this->produce();

		// テンプレートレンダリング
		$this->render();

		$this->afterFilter();

	}


	/**
	 * モードによる振り分け処理
	 * 
	 */
	function produce() {
		switch($this->checkSubmitName()) {
			case 'confirm':
				$result = $this->confirm();
				break;
			case 'retry_input':
				$this->retryInput();
				break;
			case 'send':
				$this->send();
				break;
			default:
				$this->modeFormat();
				break;
		}
	}

	/**
	 * 各処理の結果、導きだされた$processによってレンダリング先を振り分け
	 * 
	 */
	function render() {
		switch($this->getProcess()) {
			case 'index':
				$this->index();
				break;
			case 'error':
				$this->error();
				break;
			case 'preview':
				$this->preview();
				break;
			case 'thanks':
				$this->thanks();
				break;
			case 'senderror':
				$this->senderror();
				break;
			default:
				$this->index();
				break;
		}
	}

	/**
	 * 最終的なHTMLデータを出力する
	 * 
	 */
	function display($tpl_name) {
		// 表示するHTMLデータを一度フェッチする
		$output = $this->Smarty->fetch($tpl_name);

		// プロパティにセット
		$this->setOutput($output);

		// エンコード処理にかける
		$to_encoding = $this->getCharEncodeType();
		$from_encoding = $this->getFileEncodeType();
		$output = $this->getOutput();

		mb_convert_variables($to_encoding, $from_encoding, $output);

		// エンコード後のデータをプロパティに再度セット
		$this->setOutput($output);

		// フィルターにかける
		$this->afterFilter();

		// HTMLデータの出力
		echo $this->getOutput();

		// デバッグデータの出力
		$this->displayDebug();
	}

//////////////////////////////////////////////////////////////////////////////////
////// メソッド（実処理）
//////////////////////////////////////////////////////////////////////////////////

	/**
	 * ポストパラメータの検証を行い、エラー画面または
	 * 確認画面へ遷移させる
	 */
	function confirm() {
		// config.phpからバリデーションルールを呼び出し
		global $setting_list;

		// ポストパラメータをセッションに格納
		$this->postToSession();

		// バリデーションクラスの呼び出し
		require_once LIB_DIR . 'Class.Validator.php';
		$validator = new Validator($_SESSION, $setting_list);

		// バリデーション結果の取得
		$error_count = $validator->getErrorCount();
		$error_list = $validator->getErrorResults();
		$error_result = $validator->getErrorList();
		$error_message = $validator->getErrorMessages();

		// エラーのカウントが1以上の場合はエラー画面へ
		if ( $error_count > 0 ) {

			// $this->errorsに配列でエラー結果を格納
			$errors = array('errors' => array(
								'count' => $error_count,
								'list' => $error_list,
								'result' => $error_result,
								'message' => $error_message,
							));
			$this->setErrors($errors);

			
			// エラー画面へ
			$this->setProcess('error');
		} else {
			if (USE_PREVIEW) {
				// プレビュー画面へ
				$this->setProcess('preview');
			} else {
				$this->setMode('send');
				$this->send();
			}
		}
	}

	/**
	 * 戻る処理
	 * 
	 */
	function retryInput() {
		// ポストパラメータをセッションに格納
		$this->postToSession();
		$this->setMode('confirm');
		$this->setProcess('index');
	}

	/**
	 * 送信処理
	 * 
	 */
	function send() {
		// モードがsendになっていない場合は不正処理としてモード初期化
		if ($this->getMode() != 'send') {
			$this->modeFormat();
			return;
		}
		// 連続投稿チケット機能がONかつCookieにチケットが残っていた場合はsenderrorを渡して処理終了
		if ($this->hasTicket()) {
			$this->setProcess('senderror');
			return;
		}
		// config.phpの設定情報を呼び出す
		global $to;
		global $cc;
		global $bcc;

		// インスタンスの生成
		require_once LIB_DIR . 'jphpmailer.php';
		$phpmailer = new JPHPMailer();
		$this->setPhpmailer($phpmailer);
		$log = Log::getInstance();
		$log->save_dir = DATA_FILE_PATH;

		if (USE_COUNT) {
			// 	カウントの記録
			$count = $log->saveCount();
			$count = sprintf('%05d', $count);
		} else {
			$count = '';
		}

		// createReserveTagsに追加する用の配列
		$add_tags = array(
			'COUNT' => $count,
		);
		// セッションデータをメール本文テンプレートにアサインする
		// アサインするデータを配列にまとめる
		$assignParam = array(
			$_SESSION,
			$this->createReserveTags($add_tags),
		);
		$this->assignGetSessionParam();
		$this->assignArray($assignParam);

		// メールのタイトルを取得する
		$mail_title = $this->fetchMailTemplate(MAIL_TITLE_TPL_PATH);
		// メールの本文を取得する
		$mail_body = $this->fetchMailTemplate(MAIL_BODY_TPL_PATH);
		// メールの差出人アドレスを取得する
		$mail_from_mailaddr = $this->fetchMailTemplate(MAIL_FROM_MAILADDR_TPL_PATH);
		// メールの差出人名
		$mail_fromname = $this->fetchMailTemplate(MAIL_FROMNAME_TPL_PATH);

		// 管理者へのメール送信処理
		$send_result = $this->sendMail($to, $mail_from_mailaddr, $mail_title, $mail_body, array('fromname'=>$mail_fromname, 'cc'=>$cc, 'bcc'=>$bcc), true);

		if ($send_result) {
			unset($_SESSION['__mode__'], $_SESSION['__submit__']);
			$log->savePost($_SESSION);
		}

		// 自動送信メール処理
		if (AUTO_REPLY_FLAG) {
			$auto_reply_mail_title = $this->fetchMailTemplate(AR_TITLE_TPL_PATH);
			$auto_reply_mail_body = $this->fetchMailTemplate(AR_BODY_TPL_PATH);
			$auto_reply_mail_from_mailaddr = $this->fetchMailTemplate(AR_FROM_MAILADDR_TPL_PATH);
			$auto_reply_mail_fromname = $this->fetchMailTemplate(AR_FROMNAME_TPL_PATH);
			$this->sendMail($_SESSION[EMAIL], $auto_reply_mail_from_mailaddr, $auto_reply_mail_title, $auto_reply_mail_body , array('fromname'=>$auto_reply_mail_fromname), false);

		}
		// 連続投稿防止用cookieの発行
		$this->ticketIssue();

		// 次工程のプロセスセット
		$this->setProcess('thanks');

	}

	function fetchMailTemplate($file_path) {
		return trim($this->Smarty->fetch($file_path));
	}

	/**
	 * モード初期化
	 * 
	 */
	function modeFormat() {
		// 何らかの事情でセッションを持っていても強制的に破棄する
		$this->sessionFormat();
		
		// インデックスページを表示
		$this->setProcess('index');
	}

//////////////////////////////////////////////////////////////////////////////////
////// メソッド（レンダリング）
//////////////////////////////////////////////////////////////////////////////////

	/**
	 * インデックス処理
	 */
	function index() {
		// config.phpより呼び出し
		global $form_elements;

		// セッションの再設定
		$this->setMode('confirm');

        // フォームエレメントのアサイン
        $this->Smarty->assign('form_elements', $form_elements);
        
        // アサインするデータを配列にまとめる
		$assignParam = array(
			$_SESSION,
			$this->createHiddenParam(),
		);
		$this->assignGetSessionParam();
		$this->assignArray($assignParam);

		// HTMLデータの表示
		$this->display(INDEX_TEMPLATE);
	}

	/**
	 * エラー処理
	 * 
	 * @author   Suguru Chiba
	 */
	function error() {
		// config.phpより呼び出し
		global $form_elements;

		// セッションの再設定
		$this->setMode('confirm');
		// バリデーション結果を取得
		$errors = $this->getErrors();
		// Smartyオブジェクトの取得



        // フォームエレメントのアサイン
        $this->Smarty->assign('form_elements', $form_elements);

        // アサインするデータを配列にまとめる
		$assignParam = array(
			$_SESSION,
			$errors,
			$this->createHiddenParam(),
		);
		$this->assignGetSessionParam();

		$this->assignArray($assignParam);

		// HTMLデータの表示
		$this->display(ERROR_TEMPLATE);
	}

	/**
	 * 入力確認処理
	 * 
	 * @author   Suguru Chiba
	 */
	function preview() {
		// セッションの再設定
		$this->setMode('send');

		// アサインするデータを配列にまとめる
		$assignParam = array(
			$_SESSION,
			$this->createHiddenParam(),
		);
		$this->assignGetSessionParam();

		$this->assignArray($assignParam);

		// HTMLデータの表示
		$this->display(PREVIEW_TEMPLATE);
	}

	/**
	 * 送信完了処理
	 * 
	 * @author   Suguru Chiba
	 */
	function thanks() {
		// セッションの破棄
		$this->sessionFormat();
		// 送信完了画面へ遷移
		header('Location: ' . THANKS_URL);
		exit;
//		$this->display(THANKS_TEMPLATE);

	}

	/**
	 * 送信エラー
	 * 
	 * @author   Suguru Chiba
	 */
	function senderror() {
		$this->display(SEND_ERROR_TEMPLATE);

	}

//////////////////////////////////////////////////////////////////////////////////
////// メソッド（処理）
//////////////////////////////////////////////////////////////////////////////////
	/**
	 * セッションの開始
	 */
	function sessionStart() {
		session_name($this->getSessionName());
		session_cache_limiter('none');
		session_start();
		// セッションハイジャック対策
		session_regenerate_id();
	}

	/**
	 * セッションの初期化
	 */
	function sessionFormat() {
		session_unset();
		// Caution!! : ↓の処理がSID発行の阻害になるかもしれません。
		// セッションクッキーがあればそれも削除
		if (isset($_COOKIE[session_name()])) {
			setcookie(session_name(), '', time()-42000, '/');
		}
		@session_destroy();
	}

	/**
	 * セッションの中にPOSTデータを格納する
	 */
	function postToSession() {
		$post = $this->getPost();
		// アサインするデータを配列にまとめる
		foreach ($post as $key=>$value) { 
			$_SESSION[$key] = $value;
		}
	}


	/**
	 * ポストデータにモードの指定がされているかチェックする
	 *
	 * @author   Suguru Chiba
	 */
	function checkSubmitName() {
		$post = $this->getPost();

		// submitボタンが押されているかチェック
		if (isset($post[SUBMIT_NAME.'_x']) || 
			isset($post[SUBMIT_NAME.'_y']) || 
			isset($post[SUBMIT_NAME])
		) {
			return 'confirm';

		} elseif (isset($post[RETRY_INPUT]) ||
				isset($post[RETRY_INPUT.'_x']) ||
				isset($post[RETRY_INPUT.'_y'])
		) {
			// 戻るボタンの処理
			return 'retry_input';

		} elseif (isset($post[SEND_NAME]) || 
				isset($post[SEND_NAME.'_x']) || 
				isset($post[SEND_NAME.'_y'])
		) {
			// 送信処理へ
			return 'send';
		} else {
			// 規定に沿わないパラメータあるいは空のパラメータなのでindexへ戻す
			$this->sessionFormat();
			return null;
		}
	}

	/**
	 * 連想配列をSmartyテンプレートへアサインする
	 * 
	 */
	function assignArray($array) {
		foreach ($array as $key=>$value) {
			$this->Smarty->assign($value);
		}
	}

	/**
	 * フォームに落とすhiddenパラメータを格納する
	 * 
	 */
	function createHiddenParam() {
		// チェックボックス、ラジオボタン、ドロップダウンに対して空のhiddenパラメータを送信する
		if ($this->getProcess() != 'preview') {
			// チェックボックス、ラジオボタン、ドロップダウン要素がある配列のキーを取得する
			global $form_elements;
			$keys = array_keys($form_elements);
			$hidden_param = $this->createHiddenHtmlTags($keys);
		} else {
			$hidden_param = array();
		}

		// 文字コード検出用パラメータ
		// すでにdetect_encodingというセッションが入っていた場合に、上書きできない？ので一度アンセット
		unset($_SESSION['detect_encoding']);

		$hidden_param[] = '<input type="hidden" name="detect_encoding" value="あ" />';

		$hidden_param = implode("\n", $hidden_param);

		return array('hidden_param' => $hidden_param);
	}

	/**
	 * 配列で渡されたキーをname値にした空のhiddenタグを生成する
	 * return array('<input type="hidden" name="hoge" value="" />', '<input type="hidden" name="huga" value="" />')
	 */
	function createHiddenHtmlTags($keys) {
		$html_tags = array();
		foreach ($keys as $key) {
			$html_tags[] = '<input type="hidden" name="' . $key . '" value="" />';
		}
		return $html_tags;
	}

	/**
	 * Cookieがセット出来ない場合の対応
	 * 
	 */
	function assignGetSessionParam() {
		if (SID != "") {
			$this->Smarty->assign('GETSESSID', '?' . htmlspecialchars(SID, ENT_QUOTES));
		}
	}

	/**
	 * メール本文へアサインする日時、ホスト名などを取得し、配列で返す
	 * 引数の配列で追加してもよい
	 */
	function createReserveTags($add_tags=array()) {
		// エントリー日時の情報取得
		list($entry_date, $entry_time, $youbi) = Util::datePicker();

		// ユーザ情報を取得
		$ua = $_SERVER['HTTP_USER_AGENT'];
		$ip_addr = $_SERVER['REMOTE_ADDR'];
		$host = gethostbyaddr($ip_addr);

		// 予約タグのデータをアサインする
		$reserve_tags = array(
			'DATE' => $entry_date ,
			'TIME' => $entry_time,
			'YOUBI' => $youbi,
			'UA' => $ua ,
			'IP' => $ip_addr ,
			'HOST' => $host ,
		);

		foreach ($add_tags as $k=>$v) {
			$reserve_tags[$k] = $v;
		}
		return $reserve_tags;
	}


//////////////////////////////////////////////////////////////////////////////////
////// メソッド（メール送信）
//////////////////////////////////////////////////////////////////////////////////
	/**
	 * メール送信処理を行う（振り分け）
	 * 
	 */
	function sendMail($to, $from, $subject='', $body='', $options=array(), $addCcBcc=true) {
		switch (MAIL_ENCODE_TYPE) {
			case 'iso-2022-jp':
				$result = $this->_sendMail($to, $from, $subject, $body, $options, $addCcBcc);
				break;
			case 'UTF-8':
			case 'utf-8':
			case 'utf8':
				$result = $this->_sendMailUtf8($to, $from, $subject, $body, $options, $addCcBcc);
				break;
			default:
				$result = $this->_sendMail($to, $from, $subject, $body, $options, $addCcBcc);
				break;
		}
		return $result;
	}

	/**
	 * メール送信処理を行う
	 * 
	 */
	function _sendMail($to, $from, $subject='', $body='', $options=array(), $addCcBcc=true) {
		// インスタンスの生成
		$phpmailer = $this->getPhpmailer();

		// メール送信処理
		// $toの設定
		$phpmailer->addTo($to);
		if ($addCcBcc) {
			// $cc,$bccの設定
			if (!empty($options['cc'])) {
				$cc = explode(',', $options['cc']);
				foreach ($cc as $v) {
					$phpmailer->addCc($v);
				}
			}
			if (!empty($options['bcc'])) {
				$bcc = explode(',', $options['bcc']);
				foreach ($bcc as $v) {
					$phpmailer->addBcc($v);
				}
			}
		}
		// Fromの設定
		if (isset($options['fromname']) && !empty($options['fromname'])) {
			$phpmailer->setFrom($from, $options['fromname']);
		} else {
			$phpmailer->setFrom($from, $from);
		}

		$phpmailer->setSubject($subject);

		$phpmailer->setBody($body);

		$result = $phpmailer->Send();

		// phpmailerにセットされた情報をクリアする
		$phpmailer->ClearAllRecipients();

		return $result;
	}

	/**
	 * メール送信処理を行う（UTF-8）
	 * 多言語対応
	 */
	function _sendMailUtf8($to, $from, $subject='', $body='', $options=array(), $addCcBcc=false) {
		// 言語や文字コードの設定
		mb_language('uni');
		mb_internal_encoding("UTF-8");

		// Fromの設定
		if (isset($options['fromname']) && !empty($options['fromname'])) {
			$mailfrom = mb_encode_mimeheader($options['fromname']) . '<' . $from . '>';
		} else {
			$mailfrom = $from . '<' . $from . '>';
		}

		
		$headers = 'From: ' . $mailfrom . "\n";
		if ($addCcBcc) {
			// $cc,$bccの設定
			if (!empty($options['cc'])) {
				$headers .= 'Cc: ' . $options['cc'] . "\n";
			}
			if (!empty($options['bcc'])) {
				$headers .= 'Bcc: ' . $options['bcc'];
			}
		}
		//送信処理
		return mb_send_mail($to, $subject, $body, $headers);
	}

	/**
	 * 連続投稿防止チケットのチェックを行う
	 * 
	 */
	function hasTicket() {
		if (SEND_TICKET && isset($_COOKIE[TICKET_TOKEN])) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * チケットを発行する
	 * 
	 */
	function ticketIssue() {
		if (SEND_TICKET) {
			setcookie(TICKET_TOKEN , 'forbidden' , time() + SEND_TICKET);
		}
	}

	/**
	 * デバッグモードの時に各種パラメータを表示する
	 * dBug.php利用 @ http://dbug.ospinto.com/
	 * 
	 */
	function displayDebug() {
		// config.phpの定数DEBUGの値がfalseに値するものであれば何もしない
		if (DEBUG == false) return;

		// エラー結果の取得
		$errors = $this->getErrors();

		$session = $_SESSION;
		$post = $this->getPost();

		// デバッグする変数の設定
		$debug = array(
			'CurrentMode' => $this->getMode(),
			'CurrentProcess' => $this->getProcess(),
			'$_POST' => $post,
			'$_SESSION' => $session,
			'$_COOKIE' => $_COOKIE,
			'$_FILES' => $_FILES,
		);
		$debug['Errors'] = (isset($errors['errors'])) ? $errors['errors'] : null;

		// エンコーディング
		$to_encoding = $this->getCharEncodeType();
		$from_encoding = $this->getFileEncodeType();
		mb_convert_variables($to_encoding, $from_encoding, $debug);

		// dBug.phpの呼び出し
		require_once(LIB_DIR . 'dBug.php');
		new dBug($debug);
	}
} // class MailForm end