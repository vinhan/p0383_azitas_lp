<!--{* /////////////////////////////////////////////////////////////////////////////////////
□□□□□□□□□□□□□□[管理者への送信メールテンプレート]□□□□□□□□□□□□□□□
////  ここはコメントアウトの領域です。こちらに説明文を加えておきます。                  ////
////  このコメント欄を消去する必要はありません。                                        ////
////  このメールテンプレートはSmartyエンジンによるタグの利用が可能です。                ////
////  ifやsection、foreachといった分岐やループ処理も可能です。                          ////
////                                                                                    ////
////  開始タグ'<!--{'と終了タグ'--}>'の間には、"$" + INPUT入力欄のname値を入れます。    ////
////                                                                                    ////
////  その他、事前に予約されているタグがあります。                                      ////
////    <!--{$UA}-->    : 送信者のユーザーエージェントを出力します。                    ////
////    <!--{$IP}-->    : 送信者のIPアドレスを出力します。                              ////
////    <!--{$HOST}-->  : 送信者のホストを出力します。                                  ////
////    <!--{$DATE}-->  : 送信日時を出力します。                                        ////
////    <!--{$TIME}-->  : 送信時間を出力します。                                        ////
////    <!--{$YOUBI}--> : 送信した日の曜日を出力します。                                ////
////    <!--{$COUNT}--> : 受付番号を出力します（config.phpでUSE_COUNTをONにする）       ////
////    ※上記予約タグの値はname値に利用しないで下さい。                                ////
/////////////////////////////////////////////////////////////////////////////////////// *}-->
<!--{* ↓↓↓↓↓↓↓↓↓↓↓↓↓[メールテンプレートここから]↓↓↓↓↓↓↓↓↓↓↓↓↓ *}-->
ITWEEK春2020ご商談の先行予約お申し込みがありました。

■お申込みがあったLP
https://azitas.co.jp/lp/renewal/

──以下お申し込み内容──

■お名前
<!--{$name}-->

■会社名
<!--{$company}-->

■電話番号
<!--{$tel}-->

■メールアドレス
<!--{$email}-->

■商談希望形態
<!--{foreach from=$paper item="val"}-->
<!--{$val}-->
<!--{/foreach}-->

■ご相談内容
<!--{foreach from=$consult item="val"}-->
<!--{$val}-->
<!--{/foreach}-->

相談したいサイトURL
<!--{$url}-->

──────────ここまで─────────

<!--{* ↑↑↑↑↑↑↑↑↑↑↑↑↑[メールテンプレートここまで]↑↑↑↑↑↑↑↑↑↑↑↑↑ *}-->