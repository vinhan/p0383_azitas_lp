<?php /* Smarty version 2.6.26, created on 2020-03-30 13:45:02
         compiled from confirm.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'confirm.tpl', 32, false),array('modifier', 'nl2br', 'confirm.tpl', 64, false),)), $this); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="content-script-type" content="text/javascript" />
<meta http-equiv="content-style-type" content="text/css" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="description" content="株式会社アジタスが、第29回 Japan IT Week【春】に出典します！下半期最後の大商談会です。本ページからのご商談のご予約をいただいた方にもれなく「選べる！Webサイト診断レポート」をプレゼントいたします。お申し込み期限は2020/3/19まで。ぜひお早めにお申し込みください。" />
<meta name="keywords" content="" />
<title>Webマーケティング支援のアジタス Web販促ITWeek出展のご案内</title>
<link href="assets/css/style.css" rel="stylesheet">
<link href="assets/css/index.css" rel="stylesheet">
</head>
<body id="confirm">
<div id="container">
	<div id="main" class="l-main">
		<section id="main" class="p-topConfirm">
			<h1 class="p-topForm__title">お問い合わせフォーム</h1>
			<section class="p-topForm" id="survey">
				<div class="l-container">
					<h2 class="p-topForm__title p-topForm__title--confirm">入力内容確認</h2>
					<p class="p-topForm__txt01">入力内容をご確認の上、この内容でよろしければ「送信する」ボタンを押してください。<br>修正される場合は、お手数ですが「修正する」ボタンを押して、もう一度ご入力をお願いいたします。</p>
					<div class="p-form">
						<div class="p-form_in">
							<form action="./#survey" method="post" class="mailForm">
								<?php echo $this->_tpl_vars['hidden_param']; ?>

								<table class="p-form_tbl">
									<tbody>
										<tr>
											<th class="p-form_tbl_cell is-head"><span class="is-label">お名前</span></th>
											<td class="p-form_tbl_cell"><p class="p-form_txt03"><?php echo ((is_array($_tmp=$this->_tpl_vars['name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</p></td>
										</tr>
										<tr>
											<th class="p-form_tbl_cell is-head"><span class="is-label">会社名</span></th>
											<td class="p-form_tbl_cell"><p class="p-form_txt03"><?php echo ((is_array($_tmp=$this->_tpl_vars['company'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</p></td>
										</tr>
										<tr>
											<th class="p-form_tbl_cell is-head"><span class="is-label">電話番号</span></th>
											<td class="p-form_tbl_cell"><p class="p-form_txt03"><?php echo ((is_array($_tmp=$this->_tpl_vars['tel'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</p></td>
										</tr>
										<tr>
											<th class="p-form_tbl_cell is-head"><span class="is-label">メールアドレス</span></th>
											<td class="p-form_tbl_cell"><p class="p-form_txt03"><?php echo ((is_array($_tmp=$this->_tpl_vars['email'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</p></td>
										</tr>
										<tr>
											<th class="p-form_tbl_cell is-head">
												<span class="is-label"><label for="fTel">商談希望形態</label></span>
											</th>
											<td class="p-form_tbl_cell">
												<p class="p-form_txt03">
												<?php $_from = $this->_tpl_vars['paper']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['val']):
?>
												<?php echo $this->_tpl_vars['val']; ?>
<br>
												<?php endforeach; endif; unset($_from); ?>
												</p>
											</td>
										</tr>

										<tr class="trInquiry">
											<th class="p-form_tbl_cell is-head"><span class="is-label">ご相談内容</span></th>
											<td class="p-form_tbl_cell">
											<p class="p-form_txt03">
											<?php $_from = $this->_tpl_vars['consult']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['val']):
?>
											<?php echo ((is_array($_tmp=$this->_tpl_vars['val'])) ? $this->_run_mod_handler('nl2br', true, $_tmp) : smarty_modifier_nl2br($_tmp)); ?>
<br>
											<?php endforeach; endif; unset($_from); ?></p>
											</td>
										</tr>
										<tr class="trInquiry">
												<th class="p-form_tbl_cell is-head"><span class="is-label">相談したいサイトURL</span></th>
												<td class="p-form_tbl_cell"><p class="p-form_txt03"><?php echo ((is_array($_tmp=$this->_tpl_vars['url'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</p></td>
											</tr>
									</tbody>
								</table>
								<ul class="p-form_submit">
									<li class="p-form_submit_btn">
										<button name="__retry_input__" class="c-btn04 c-btn04--confirm">
											<a><span class="p-form_submit_btn_txt2">修正する</span></a>
										</button>
									</li>
									<li class="p-form_submit_btn">
										<button name="__send__" class="c-btn04 c-btn04--submit">
											<a><span class="p-form_submit_btn_txt3">送信する</span></a>
										</button>
									</li>
								</ul>
							</form>
						</div>
					</div>
				</div>
			</section>
		</section>
		<!-- conts end-->
	</div>
	<!-- /#main -->

	<footer class="c-footer">
			<div class="l-container">
				<div class="c-footer__inner">
					<div class="c-footer__logo">
						<a href="https://azitas.co.jp/"><img src="assets/img/118.png" width="104" height="27" alt="Azitas"></a>
						<a href="https://azitas.co.jp/privacy.php">プライバシーポリシー</a>
					</div>
					<p class="c-footer__copy">Copyright (c) Azitas. All Rights Reserved.</p>
				</div>
			</div>
			<div class="c-backtop"><img src="assets/img/119.png" width="80" height="92" alt="pagetopボタン"></div>
		</footer>
</div>
<?php  include($_SERVER['DOCUMENT_ROOT']."/inc_file/tags_bottom.php");  ?>

<script src="assets/js/common.js"></script>
<script src="assets/js/index.js"></script>
</body>

</html>