<?php /* Smarty version 2.6.26, created on 2020-03-30 13:45:13
         compiled from /home/test20008/test20008.com/public_html/open2019/lp/renewal/./mail_tpl/ar_mail_body.txt */ ?>
﻿<?php echo $this->_tpl_vars['name']; ?>
様

この度は、ITWEEK2020春ご商談の先行予約お申し込みをいただきありがとうございます。
分析レポートは商談当日にお渡しいたします。


──以下、ご回答いただきましたご商談の先行予約申し込み内容です──

■お名前
<?php echo $this->_tpl_vars['name']; ?>


■会社名
<?php echo $this->_tpl_vars['company']; ?>


■電話番号
<?php echo $this->_tpl_vars['tel']; ?>


■メールアドレス
<?php echo $this->_tpl_vars['email']; ?>


■商談希望形態
<?php $_from = $this->_tpl_vars['paper']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['val']):
?>
<?php echo $this->_tpl_vars['val']; ?>

<?php endforeach; endif; unset($_from); ?>

■ご相談内容
<?php $_from = $this->_tpl_vars['consult']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['val']):
?>
<?php echo $this->_tpl_vars['val']; ?>

<?php endforeach; endif; unset($_from); ?>

相談したいサイトURL
<?php echo $this->_tpl_vars['url']; ?>


──────────ここまで─────────

特典やご不明点など、
何かございましたら下記メールアドレス宛にご連絡下さい。
なお、本メールへの返信は受け付けておりませんのでご了承下さい。

※当社の定休日にお問い合わせいただいた場合は、翌営業日以降の対応とさせていただきます。
　恐れ入りますが、弊社は土日祝日はお休みをいただいております。

──────────────────────────

株式会社アジタス
〒102-0072
東京都千代田区飯田橋3-2-2 飯田橋3丁目ビル3F
TEL　　 ：03-6416-1058
電話受付：10：00～18：00（※土日祝日を除く）
E-MAIL　：azitas-pr@azitas.co.jp
URL　　 ：https://azitas.co.jp/
