<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
	<meta name="format-detection" content="telephone=no">
	<meta name="description" content="株式会社アジタスが、第29回 Japan IT Week【春】に出典します！下半期最後の大商談会です。本ページからのご商談のご予約をいただいた方にもれなく「選べる！Webサイト診断レポート」をプレゼントいたします。お申し込み期限は2020/3/19まで。ぜひお早めにお申し込みください。">
	<meta name="keywords" content="">
	<title>Webマーケティング支援のアジタス Web販促ITWeek出展のご案内</title>
	<link href="assets/css/common.css" rel="stylesheet">
	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/index.css" rel="stylesheet">
</head>


<body>

	<div id="container">
		<header id="header" class="c-header">
			<div class="c-header__logo">
				<a href="/lp/renewal/">
					<img class="pc-only" src="assets/img/100.png" width="244" height="27" alt="Azitas 展示会特設サイト">
					<img class="sp-only" src="assets/img/100_sp.png" width="50" height="28" alt="Azitas 展示会特設サイト">
				</a>
			</div>
		</header><!-- /header -->

		<div id="main" class="p-thank">
			<div class="l-container">
				<div class="p-thank__inner">
					<h2 class="p-thank__title"><span>ありがとうございます</span>お申し込みが完了しました</h2>
					<p class="p-thank__txt">ご入力いただいたメールアドレス宛に<br class="sp-only">ご確認メールを送信いたしました。<br>メールが届かない場合や、<br class="sp-only">その他ご不明点がある場合は、<br>大変お手数ですが <span>03-6416-1058</span> まで<br class="sp-only">お電話ください。</p>
					<h3 class="p-thank__title02"><span>こちらも合わせてご覧ください</span></h3>
					<ul class="p-thank__list">
						<li><a href="https://azitas.co.jp/markesemi/" target="_blank">
							<img class="pc-only" src="assets/img/121.png" width="482" height="191" alt="Webマーケの本質を知りたいあなたのための学びの場 マーケティングゼミナール">
							<img class="sp-only" src="assets/img/121_sp.png" width="483" height="219" alt="Webマーケの本質を知りたいあなたのための学びの場 マーケティングゼミナール">
						</a></li>
						<li><a href="https://sirolab.jp" target="_blank">
							<img class="pc-only" src="assets/img/122.png" width="482" height="191" alt="ホワイトペーパーの活用法を基礎から学べるサイト">
							<img class="sp-only" src="assets/img/122_sp.png" width="489" height="219" alt="ホワイトペーパーの活用法を基礎から学べるサイト">
						</a></li>
					</ul>
					<div class="c-btn02 c-btn02--thanks">
						<a href="/lp/renewal/"><span><strong>TOPへ戻る</strong></span></a>
					</div>
				</div>
			</div>
		</div>

		<footer class="c-footer">
			<div class="l-container">
				<div class="c-footer__inner">
					<div class="c-footer__logo">
						<a href="https://azitas.co.jp/"><img src="assets/img/118.png" width="104" height="27" alt="Azitas"></a>
						<a href="https://azitas.co.jp/privacy.php">プライバシーポリシー</a>
					</div>
					<p class="c-footer__copy">Copyright (c) Azitas. All Rights Reserved.</p>
				</div>
			</div>
			<div class="c-backtop"><img src="assets/img/119.png" width="80" height="92" alt="pagetopボタン"></div>
		</footer>
	</div>

	<script src="assets/js/jquery-3.3.1.min.js"></script>
	<script src="assets/js/jquery.matchHeight.min.js"></script>
	<!-- <script src="assets/js/common.js"></script> -->
	<script src="assets/js/index.js"></script>
	<script src="assets/js/functions.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-15601782-2"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());
 gtag('config', 'UA-15601782-2');
</script>


</body>
</html>