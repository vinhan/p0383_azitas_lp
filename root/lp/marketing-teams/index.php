<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
	<meta name="format-detection" content="telephone=no">
	<meta name="description" content="株式会社アジタスが、第29回 Japan IT Week【春】に出典します！下半期最後の大商談会です。本ページからのご商談のご予約をいただいた方にもれなく「選べる！Webサイト診断レポート」をプレゼントいたします。お申し込み期限は2020/03/19まで。ぜひお早めにお申し込みください。">
	<meta name="keywords" content="">
	<title>Webマーケティング支援のアジタス Web販促ITWeek出展のご案内</title>
	<!-- <link href="/lp/marketing-teams/assets/css/common.css" rel="stylesheet"> -->
	<link href="/lp/marketing-teams/assets/css/style.css" rel="stylesheet">
	<link href="/lp/marketing-teams/assets/css/index.css" rel="stylesheet">
</head>
<body>
	<div id="container">
		<header id="header" class="c-header">
			<div class="c-header__logo">
				<a href="/lp/renewal/">
					<img class="pc-only" src="/lp/marketing-teams/assets/img/100.png" width="244" height="27" alt="Azitas 展示会特設サイト">
					<img class="sp-only" src="/lp/marketing-teams/assets/img/100_sp.png" width="50" height="28" alt="Azitas 展示会特設サイト">
				</a>
			</div>
		</header><!-- /header -->

		<div id="main" class="p-top">
			<section class="p-top01">
				<div class="p-top01__inner">
					<div class="p-top01__img">
						<img src="/lp/marketing-teams/assets/img/img_101.png" alt="こんな時代を乗り切るために Webで売上を上げる 方法が分からない方へ 専属チームが支援します! 「事業としてのWeb、Webを活用した事業の推進」を実現します。 新型コロナウイルス感染症（COVID-19）の影響で、従来のような「オフラインでの活動」「対面での営業」が大きく制限を受けるようになりお困りの方も多いのではないでしょうか。「Webを活用した営業」に切り替えたい。そんな急な状況の変化に悩む企業をWenマーケティング全般のサポートをしているアジタスが支援します！">
					</div>
				</div>
			</section><!-- /p-top01 -->
			<section class="p-topCV">
				<div class="l-container">
					<h2 class="p-topCV__title">
						<img src="/lp/marketing-teams/assets/img/img_102.png" alt="ご商談の先行予約はこちら">
					</h2>

					<ul class="p-topCV__list">
						<li>
							<img src="/lp/marketing-teams/assets/img/img_103.png" alt="総合分析レポート 01 自社サイトを分析したことがない方にオススメ！総合的にサイトの現状をお調べします。 競合分析レポート 02 競合サイトと貴社のサイトを比較した結果をお調べします！比較したい競合サイトのURLをお知らせください。 ニーズ分析レポート 03 どんなキーワードから流入があり、どんなコンテンツが閲覧されているのかなどをお調べします！">

						</li>

						<li>
							<figure class="p-topCV__imgTxt">
								<img src="/lp/marketing-teams/assets/img/img_104.png" alt="総合分析レポート 01 自社サイトを分析したことがない方にオススメ！総合的にサイトの現状をお調べします。 競合分析レポート 02 競合サイトと貴社のサイトを比較した結果をお調べします！比較したい競合サイトのURLをお知らせください。 ニーズ分析レポート 03 どんなキーワードから流入があり、どんなコンテンツが閲覧されているのかなどをお調べします！">
							</figure>

							<!-- <div class="c-btn02">
								<a href="#survey"><span><strong>予約する</strong></span></a>
							</div> -->

							<!-- <p class="p-topCV__txt">
								※ご予約なしでも当日のご商談は受け付けておりますが、先着順となりますので商談予約の申し込みを<br class="pc-only">　オススメいたします。<br class="sp-only">※分析レポートは商談当日にお渡しいたします。</p> -->
						</li>
					</ul>

					<div class="p-topCV__info">
						<h3 class="p-topCV__info--ttl"><img src="/lp/marketing-teams/assets/img/txt_101.png" alt="\  このページからお問い合わせいただいた方にプレゼント  /"></h3>
						<div class="p-topCV__info--box">
							<p class="p-topCV__info--txt">
							Webサイトの分析・設計・制作からWebマーケティングの戦略策定まで、お気軽にご相談ください。<br/>
							最適な施策をご提案いたします。
							</p>
							<div class="c-btn01">
								<a href="#survey"><span>お問い合わせはこちら</span></a>
							</div>
						</div>
					</div>
				</div>
			</section><!-- /p-topCV -->
			<section class="p-top02">
				<div class="p-top02__mid">
					<div class="l-container">
						<div class="p-top02__mid01">
							<h2 class="p-top02__mid-title"><img src="/lp/marketing-teams/assets/img/txt_102.png" alt="Webマーケティングが上手くいかない! \ こんなお悩みありませんか？ /"></h2>
	
							<ul class="p-top02__mid-list">
								<li>
									<div class="p-top02__mid-bg">
										<div class="p-top02__mid-img">
											<figure class="p-top02__mid-img--img pc-only">
												<img src="/lp/marketing-teams/assets/img/mid01.png" alt="A社">
											</figure>
										</div>
										<div class="p-top02__mid-txt">
											Webのことわかっていそうな若手社員が、<br>
											<strong>企画から制作、運営まで全て担う「一人Web担」</strong>になっている。
										</div>
									</div>
								</li>
								<li>
									<div class="p-top02__mid-bg">
										<div class="p-top02__mid-img">
											<figure class="p-top02__mid-img--img pc-only">
												<img src="/lp/marketing-teams/assets/img/mid02.png" alt="A社">
											</figure>
										</div>
										<div class="p-top02__mid-txt">
											<strong>外部業者との仕事</strong>をマネジメントできない。<br>
											発注書発行、素材支給といった<strong>事務処理だけでもかなりの手間。</strong>
										</div>
									</div>
								</li>
								<li>
									<div class="p-top02__mid-bg">
										<div class="p-top02__mid-img">
											<figure class="p-top02__mid-img--img pc-only">
												<img src="/lp/marketing-teams/assets/img/mid03.png" alt="A社">
											</figure>
										</div>
										<div class="p-top02__mid-txt">
											<strong>やるべきことが多すぎて</strong>「Webで成果を出すために何をすればよ良いか」を<strong>しっかり考える余裕がない。</strong>
										</div>
									</div>
								</li>
							</ul>
						</div>

						<div class="p-top02__mid02">
							<h2 class="p-top02__mid-title"><img src="/lp/marketing-teams/assets/img/txt_106.png" alt="事業としてのWeb活用はこんなにも難しい"></h2>

							<p class="p-top02__mid-txt">マーケティングから Web、システム、事業設計まで広範囲に及ぶプロジェクトを立ち上げる際は、戦略立案はもちろん、営業活動や全社的なリブランディング、新規事業創出など、<strong>網羅的な知識と長年の実働経験を必要とします。経験の浅い担当者がプロジェクトの全てを把握し、進行・管理することは困難です。</strong></p>

							<ul class="p-top02__mid02-list">
								<li><img src="/lp/marketing-teams/assets/img/img_txt01.jpg" alt="知っていなければならないこと マーケティング SEO 競合の動向 最新のトレンド 自社の営業戦略/店舗戦略 ブランディングWeb広告の仕組み 自社商品の強み・差別化 UI/UX 標準的施策のトレンド 関連法規 セキュリティ 施策実行の相場 各ベンダーの違い Webに用いられる技術 自社顧客管理システムの仕様 ...etc"></li>
								<li><img src="/lp/marketing-teams/assets/img/img_txt02.jpg" alt="できなければならないこと ログ解析 Web 広告のレポーティング デザインの見極め 担当者の育成 / 脱属人化 コンテンツの質の見極め / 投入プロジェクトマネジメント ベンダーマネジメント 事業戦略の落とし込み マーケティングプロセスの設計 自社システムとの連携 部門間連携 / 改変 担当者の人事査定 項目の設定 目標設定 ...etc"></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="p-top02__bot">
					<div class="l-container">
						<figure class="p-top02__bot-img1">
							<img src="/lp/marketing-teams/assets/img/img_109.png" alt="" class="pc-only">
						</figure>

						<h2 class="p-top02__bot-title">
							<img src="/lp/marketing-teams/assets/img/txt_103.png" alt="お悩みの尽きないWeb担やマーケ担当者の皆さまへ、より良い選択肢をアジタスがご提供します！">
						</h2>
						<figure class="p-top02__bot-img2">
							<img src="/lp/marketing-teams/assets/img/img_110.png" alt="" class="pc-only">
						</figure>
					</div>
				</div>
			</section><!-- /p-top02 -->
			<section class="p-top03">
				<div class="l-container">
					<div class="p-top03__01">
						<div class="p-top03__01-content">
							<h2 class="p-top03__01-title"><img src="/lp/marketing-teams/assets/img/txt_107.png" alt="マーケティングチーム外部化でWebマーケティングの成功を全面的に支援します！"></h2>
						
							<div class="p-top03__01-txt">
								<p>「マーケティングチームの外部化」とは、文字通り<strong>Webマーケティングに必要な<br class="pc-only">ノウハウやリソースを「まるごとのチームとして外部に持つ」</strong>ということです。</p>

								<p>チームのメンバーはweb担当者の手足、ときには頭脳となってくれます。「一人web担当」の課題を解決し、御社のwebマーケティング施策を強力に後押ししてくれるパートナーになるのです。</p>
							</div>
						</div>

						<div class="p-top03__01-img">
							<img src="/lp/marketing-teams/assets/img/img_txt03.png" alt="">
						</div>
					</div>

					<div class="p-top03__02">
						<h2 class="p-top03__title">マーケティングチーム外部化<i>の</i><br/><strong>４<span class="size">つの</span>メリット</strong></h2>
	
						<div class="c-list1">
							<div class="c-list1__item">
								<img src="/lp/marketing-teams/assets/img/img_115.jpg"  alt="">
								<!-- <span class="c-list1__number"><img src="/lp/marketing-teams/assets/img/number01.png" width="60" height="67" alt="01"></span>
								<div class="c-list1__title">
								<h3 class="c-list1__ttl">質の高い<strong>見込み顧客の獲得</strong></h3>
									<div class="c-list1__img">
										<img class="pc-only" src="/lp/marketing-teams/assets/img/img_111.png">
										<img class="sp-only" src="/lp/marketing-teams/assets/img/107_sp.png" width="57" height="55" alt="ホワイトペーパー制作プラン">
									</div>
								</div>
								<p class="c-list1__txt">Webマーケティングの強みの一つは、人力ではとても実現できないような大量の見込み顧客との接触が可能な点です。サイトのコンテンツを読み込んだユーザーは情報収集を行って知識を高めていくため、貴社にとっての優良顧客と成長しCVが発生しやすい環境を作り出せます。</p> -->
							</div>
							<div class="c-list1__item">
								<img src="/lp/marketing-teams/assets/img/img_116.jpg"  alt="">
								<!-- <span class="c-list1__number"><img src="/lp/marketing-teams/assets/img/number02.png" width="69" height="67" alt="02"></span>
								<div class="c-list1__title">
									<h3 class="c-list1__ttl"><strong>高速PDCA</strong>による改善</h3>
									<div class="c-list1__img">
										<img class="pc-only" src="/lp/marketing-teams/assets/img/img_112.png">
										<img class="sp-only" src="/lp/marketing-teams/assets/img/108_sp.png" width="53" height="67" alt="マーケティングチーム外部化プラン">
									</div>
								</div>
								<p class="c-list1__txt">顧客化につながる特定ターゲットへの働きかけをWeb上で行う方法は複数あり、失敗しても軽微、成功すれば横展開と拡大ができるため、PDCAを高速で回すことが可能です。展示会・セミナーの機能はWeb上で展開可能であり、結果が出るまでのタイムロスを防げます。</p> -->
							</div>
							<div class="c-list1__item">
								<img src="/lp/marketing-teams/assets/img/img_118.jpg"  alt="">
								<!-- <span class="c-list1__number"><img src="/lp/marketing-teams/assets/img/number03.png" width="68" height="67" alt="03"></span>
								<div class="c-list1__title">
									<h3 class="c-list1__ttl">単発施策から脱却し、<br/><strong>長期的資産効果</strong>をもたらす</h3>
									<div class="c-list1__img">
										<img class="pc-only" src="/lp/marketing-teams/assets/img/img_113.png" alt="">
										<img class="sp-only" src="/lp/marketing-teams/assets/img/109_sp.png" width="50" height="60" alt="Webサイト大規模リニューアルプラン">
									</div>
								</div>
								<p class="c-list1__txt">「飛び込み」「テレアポ」「DM」などの費用対効果・効率の悪い施策ではなく、「選び方」や「購入に誘導するコンテンツ」を制作することで公開後もサイトを閲覧するユーザーにリーチし続けることが可能です。過去の施策が積み上がって接触点における差別化要因になり、長期的な費用対効果が高まり、営業でも効果をアピールするソリューション提供に活かされます。</p> -->
							</div>
							<div class="c-list1__item">
								<img src="/lp/marketing-teams/assets/img/img_117.jpg"  alt="">
								<!-- <span class="c-list1__number"><img src="/lp/marketing-teams/assets/img/number04.png" width="71" height="67" alt="04"></span>
								<div class="c-list1__title">
									<h3 class="c-list1__ttl">Webと連携することで<strong><br/>見込み顧客のデータベース化</strong></h3>
									<div class="c-list1__img">
										<img class="pc-only" src="/lp/marketing-teams/assets/img/img_114.png" >
										<img class="sp-only" src="/lp/marketing-teams/assets/img/110_sp.png" width="68" height="65" alt="アクセス数急増プラン">
									</div>
								</div>
								<p class="c-list1__txt">Webサイトの役割は、見込み顧客を増大させることです。個人情報を取得できたケースもあればそうではないケースもあります。施策を通してリストを作成し、過去にどんな働きかけをしたかの情報がまとまっていれば適切かつ効率的な営業活動を行えます。見込度に合わせた対策の実施や、特殊な事例を参照できるデータベースを作り、購入する可能性の高い見込み顧客に有効的に営業リソースを使うことが可能です。</p> -->
							</div>
						</div>
					</div>
				</div>
			</section><!-- /p-top03 -->
			<section class="p-top04">
				<div class="l-container">
					<h2 class="p-top04__title">アジタス<i>が</i>提供する<br/><span>マーケティング支援内容</span></h2>
					<div class="p-support">
						<div class="p-support_in pc-only">
							<ul class="p-support_list">
								<li>ホワイトペーパー制作</li>
								<li>マーケティングオートメーション</li>
								<li>オウンドメディア運営</li>
								<li>チーム丸ごとリソース提供</li>
								<li>OtoO/リアルマーケとの連携</li>
								<li>Webブランディング</li>
								<li>マーケティング全体の設計</li>
								<li>超高速PDCAチーム</li>
								<li>制作陣のコミュニケーション整備</li>
								<li>社内整備(営業との連携、技術者同士)</li>
								<li>プロジェクトマネジメント</li>
								<li>越境EC</li>
								<li>ブログ担当者育成</li>
								<li>10万ページ以上のWebサイトリニューアル</li>
							</ul>
							<ul class="p-support_list">
								<li>アクセスアップ</li>
								<li>CVR/CPA改善</li>
								<li>LPの高速A/Bテスト</li>
								<li>アクセシビリティ・ユーザビリティ</li>
								<li>ディレクション</li>
								<li>ユーザビリティテスト</li>
								<li>ログ解析</li>
								<li>内部SEO</li>
								<li>コーディング</li>
								<li>システム開発</li>
								<li>仕様決め</li>
								<li>CMS</li>
								<li>レスポンシプ対応</li>
								<li>コーディングレギュレーションの設計</li>
								<li>レコメンドエンジン開発</li>
							</ul>
							<ul class="p-support_list">
								<li>EC・モール対応</li>
								<li>Web広告</li>
								<li>SNS対応</li>
								<li>コンテンツマーケティング</li>
								<li>エンゲージメントを高めるオリジナル<br class="pc-only">コンテンツ</li>
								<li>口コミサイト構築</li>
								<li>YouTube広告</li>
								<li>メルマガ配信システム</li>
								<li>コンテンツ企画</li>
								<li>執筆・ライティング/取材</li>
								<li>キャッチコピー</li>
								<li>動きのあるデザイン</li>
								<li class="et is-note1">︙</li>
								<li class="et is-note2">BtoB、BtoC、業種業界を問わず<br>なんでもご相談ください</li>
							</ul>
						</div>

						<div class="p-support_in sp-only">
							<ul class="p-support_list">
								<li>ホワイトペーパー制作</li>
								<li>マーケティングオートメーション</li>
								<li>オウンドメディア運営</li>
								<li>チーム丸ごとリソース提供</li>
								<li>OtoO/リアルマーケとの連携</li>
								<li>Webブランディング</li>
								<li>マーケティング全体の設計</li>
								<li>超高速PDCAチーム</li>
								<li>制作陣のコミュニケーション整備</li>
								<li>社内整備(営業との連携、技術者同士)</li>
								<li>プロジェクトマネジメント</li>
								<li>10万ページ以上の<br class="sp-only">Webサイトリニューアル</li>
								<li>越境EC</li>
								<li>ブログ担当者育成</li>
								<li>アクセスアップ</li>
								<li>CVR/CPA改善</li>
								<li>LPの高速A/Bテスト</li>
								<li>アクセシビリティ・ユーザビリティ</li>
								<li>ディレクション</li>
								<li>ユーザビリティテスト</li>
								<li>ログ解析</li>
								<li>内部SEO</li>
								<li>コーディング</li>
								<li>システム開発</li>
								<li>仕様決め</li>
								<li>CMS</li>
								<li>レスポンシプ対応</li>
								<li>コーディングレギュレーションの設計</li>
								<li>レコメンドエンジン開発</li>
								<li>EC・モール対応</li>
								<li>Web広告</li>
								<li>SNS対応</li>
								<li>コンテンツマーケティング</li>
								<li>エンゲージメントを高める<br class="sp-only">オリジナル<br class="pc-only">コンテンツ</li>
								<li>口コミサイト構築</li>
								<li>YouTube広告</li>
								<li>メルマガ配信システム</li>
								<li>コンテンツ企画</li>
								<li>執筆・ライティング/取材</li>
								<li>キャッチコピー</li>
								<li>動きのあるデザイン</li>
								<li class="et is-note1">︙</li>
								<li class="et is-note2">BtoB、BtoC、業種業界を問わず<br>なんでもご相談ください</li>
							</ul>
						</div>
					</div>
				</div>
			</section><!-- /p-top04 -->
			<section class="p-topCV">
				<div class="l-container">
					<h2 class="p-topCV__title">
						<img src="/lp/marketing-teams/assets/img/img_102.png" alt="ご商談の先行予約はこちら">
					</h2>

					<ul class="p-topCV__list">
						<li>
							<img src="/lp/marketing-teams/assets/img/img_103.png" alt="総合分析レポート 01 自社サイトを分析したことがない方にオススメ！総合的にサイトの現状をお調べします。 競合分析レポート 02 競合サイトと貴社のサイトを比較した結果をお調べします！比較したい競合サイトのURLをお知らせください。 ニーズ分析レポート 03 どんなキーワードから流入があり、どんなコンテンツが閲覧されているのかなどをお調べします！">

						</li>

						<li>
							<figure class="p-topCV__imgTxt">
								<img src="/lp/marketing-teams/assets/img/img_104.png" alt="総合分析レポート 01 自社サイトを分析したことがない方にオススメ！総合的にサイトの現状をお調べします。 競合分析レポート 02 競合サイトと貴社のサイトを比較した結果をお調べします！比較したい競合サイトのURLをお知らせください。 ニーズ分析レポート 03 どんなキーワードから流入があり、どんなコンテンツが閲覧されているのかなどをお調べします！">
							</figure>

							<!-- <div class="c-btn02">
								<a href="#survey"><span><strong>予約する</strong></span></a>
							</div> -->

							<!-- <p class="p-topCV__txt">
								※ご予約なしでも当日のご商談は受け付けておりますが、先着順となりますので商談予約の申し込みを<br class="pc-only">　オススメいたします。<br class="sp-only">※分析レポートは商談当日にお渡しいたします。</p> -->
						</li>
					</ul>

					<div class="p-topCV__info">
						<h3 class="p-topCV__info--ttl"><img src="/lp/marketing-teams/assets/img/txt_101.png" alt="\  このページからお問い合わせいただいた方にプレゼント  /"></h3>
						<div class="p-topCV__info--box">
							<p class="p-topCV__info--txt">
							Webサイトの分析・設計・制作からWebマーケティングの戦略策定まで、お気軽にご相談ください。<br/>
							最適な施策をご提案いたします。
							</p>
							<div class="c-btn01">
								<a href="#survey"><span>お問い合わせはこちら</span></a>
							</div>
						</div>
					</div>
				</div>
			</section><!-- /p-topCV -->
			<section class="p-top05">
				<div class="p-top05__01">
					<div class="l-container">
						<h2 class="p-top05__title">アジタス<i>では、</i><br/>
						<span>あらゆる悩みを抱えた方を</span><br>
						<span>サポート</span><i>しています</i></h2>
	
						<div class="p-top05__wrap">
							<div class="p-top05__heading">
								<h3 class="p-top05__heading-title">
									<img src="/lp/marketing-teams/assets/img/txt_105.png" alt="" class="pc-only">
	
								</h3>
							</div>
	
							<div class="p-top05__block p-top05__block01">
								<img src="/lp/marketing-teams/assets/img/img_125.png" alt="" >
							</div>
						</div>
					</div>
				</div>

				<div class="p-top05__02">
					<div class="l-container">
						<h2 class="p-top05__title2">お客様<i>の</i><span>声</span></h2>
						<ul class="c-list3">
							<li class="c-list3__item"><img src="/lp/marketing-teams/assets/img/img_130.png" alt="" ></li>
							<li class="c-list3__item"><img src="/lp/marketing-teams/assets/img/img_127.png" alt="" ></li>
							<li class="c-list3__item"><img src="/lp/marketing-teams/assets/img/img_128.png" alt="" ></li>
							<li class="c-list3__item"><img src="/lp/marketing-teams/assets/img/img_129.png" alt="" ></li>
						</ul>
					</div>	
				</div>
				</div>
			</section><!-- /p-top05 -->
			<section class="p-top06">
				<div class="l-container">
					<img src="/lp/marketing-teams/assets/img/img_131.png" >
				</div>
			</section><!-- /p-top06 -->
			<section class="p-top07">
				<div class="l-container">
					<div class="p-top07__img1">
						<img src="/lp/marketing-teams/assets/img/img_132.png" >
					</div>
					<div class="p-top07__img2">
						<img src="/lp/marketing-teams/assets/img/img_133.png" >
					</div>
				</div>
			</section><!-- /p-top06 -->
			<section class="p-topForm" id="survey">
				<div class="l-container">
					<h2 class="p-topForm__title">2大特典が気になる方・相談したい方はこちら</h2>
					<p class="p-topForm__txt01">お問い合わせフォーム</p>
					<div class="p-form">
						<div class="p-form_in">
							<p class="p-form_txt02"><span class="is-must">必須</span>は入力必須項目です。必ずご入力ください。</p>
							<form action="./#survey" method="post" class="mailForm">
								<!--{$hidden_param}-->
								<table class="p-form_tbl">
									<tbody>
										<tr>
											<th class="p-form_tbl_cell is-head">
												<span class="is-label"><label for="fName">お名前</label></span>
												<span class="is-cell"><span class="is-must">必須</span></span>
											</th>
											<td class="p-form_tbl_cell"><input type="text" id="fName" name="name" placeholder="例）山田 太郎" value="<!--{$name|escape}-->"></td>
										</tr>
										<tr>
											<th class="p-form_tbl_cell is-head">
												<span class="is-label"><label for="fCompany">会社名</label></span>
												<span class="is-cell"><span class="is-must">必須</span></span>
											</th>
											<td class="p-form_tbl_cell"><input type="text" id="fCompany" name="company" placeholder="例）株式会社アジタス" value="<!--{$company|escape}-->"></td>
										</tr>
										<tr>
											<th class="p-form_tbl_cell is-head">
												<span class="is-label"><label for="fTel">電話番号</label></span>
												<span class="is-cell"><span class="is-must">必須</span></span>
											</th>
											<td class="p-form_tbl_cell"><input type="tel" id="fTel" name="tel" placeholder="例）000-000-0000" value="<!--{$tel|escape}-->"></td>
										</tr>
										<tr>
											<th class="p-form_tbl_cell is-head">
												<span class="is-label"><label for="fEmail">メールアドレス</label></span>
												<span class="is-cell"><span class="is-must">必須</span></span>
											</th>
											<td class="p-form_tbl_cell"><input type="email" id="fEmail" name="email" placeholder="例）info@example.com" value="<!--{$email|escape}-->"></td>
										</tr>
										<tr>
											<th class="p-form_tbl_cell is-head">
												<span class="is-label"><label for="fTel">商談希望形態</label></span>
												<span class="is-cell"><span class="is-must">必須</span></span>
											</th>
											<td class="p-form_tbl_cell">
												<ul class="is-radio is-study">
													<!--{html_radios name="paper" options=$form_elements.paper assign="paper" checked=$paper}-->
													<li><!--{$paper.0}--></li>
													<li><!--{$paper.1}--></li>
													<li><!--{$paper.2}--></li>
												</ul>
											</td>
										</tr>
										<tr>
											<th class="p-form_tbl_cell is-head">
												<span class="is-label"><label for="fTel">ご相談内容</label></span>
												<span class="is-cell"><span class="is-must">必須</span></span>
											</th>
											<td class="p-form_tbl_cell">
												<textarea id="fConsult" name="consult" cols="5" rows="3" placeholder="自社Webサイトへのご不満、どのようなことがうまくいかないのか、サイトで実現したいことなど、なんでもお聞かせください。"><!--{$consult|escape}--></textarea>
											</td>
										</tr>
										<tr>
											<th class="p-form_tbl_cell is-head">
												<span class="is-label"><label for="fDemand">相談したいサイトURL</label></span>
												<span class="is-cell"><span class="is-any">任意</span></span>
											</th>
											<td class="p-form_tbl_cell">
												<input type="url" id="fUrl" name="url" placeholder="例）info@example.com" value="<!--{$url|escape}-->">
											</td>
										</tr>
										<!--<tr>
											<th class="p-form_tbl_cell is-head">
												<span class="is-label"><label for="fOther">その他のご相談内容</label></span>
												<span class="is-cell"><span class="is-any">任意</span></span>
											</th>
											<td class="p-form_tbl_cell"><textarea id="fOther" name="other" cols="5" rows="5" placeholder="自社Webサイトへのご不満、どのようなことがうまくいかないのか、サイトで実現したいことなど、なんでもお聞かせください。">{$other|escape}</textarea></td>
										</tr>-->
									</tbody>
								</table>
								<ul class="p-form_submit">
									<li class="p-form_submit_btn">
										<button name="__submit__" class="c-btn01 c-btn01--submit">
											<a><span>入力内容を確認する</span></a>
										</button>
										<!-- <button name="__submit__" class="p-form_submit_btn_anchor">
											入力内容を確認する
										</button> -->
									</li>
								</ul>
							</form>
						</div>
					</div>
					<p class="p-topForm__txt02">万が一メールが届かない場合や、その他ご不明な点がございましたら、<br class="pc-only">大変お手数ですが <span class="p-topForm__txt02--tel pc-only">TEL.03-6416-1058</span><a class="sp-only" href="tel:0364161058">TEL.03-6416-1058</a> へお問い合わせください。</p>
				</div>
			</section>
		</div>

		<footer class="c-footer">
			<div class="l-container">
				<div class="c-footer__inner">
					<div class="c-footer__logo">
						<a href="https://azitas.co.jp/"><img src="/lp/marketing-teams/assets/img/118.png" width="104" height="27" alt="Azitas"></a>
						<a href="https://azitas.co.jp/privacy.php">プライバシーポリシー</a>
					</div>
					<p class="c-footer__copy">Copyright (c) Azitas. All Rights Reserved.</p>
				</div>
			</div>
			<div class="c-backtop"><img src="/lp/marketing-teams/assets/img/119.png" width="80" height="92" alt="pagetopボタン"></div>
		</footer>
	</div>

	<script src="/lp/marketing-teams/assets/js/jquery-3.3.1.min.js"></script>
	<script src="/lp/marketing-teams/assets/js/jquery.yurayura.js"></script>
	<script src="/lp/marketing-teams/assets/js/jquery.matchHeight.min.js"></script>
	<script src="/lp/marketing-teams/assets/js/smoothscroll.js"></script>
	<script src="/lp/marketing-teams/assets/js/index.js"></script>
	<script src="/lp/marketing-teams/assets/js/functions.js"></script>
	<!-- {php}--><!-- include($_SERVER['DOCUMENT_ROOT']."/inc_file/tags_bottom.php");-->  <!--{/php} -->
</body>
</html>
